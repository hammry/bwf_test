﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Data : MonoBehaviour {

    public static int Score = 0;
    public static float HP = 75f;
    public static Transform Player;
    string PlayerTag = "Player";


    private void Awake()
    {
        Score = 0;
        HP = 75f;
        Player = GameObject.FindGameObjectWithTag(PlayerTag).transform;
    }

    private void Update()
    {
        HP = Mathf.Clamp(HP, 0f, 100f);
    }

}
