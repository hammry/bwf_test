﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadLevel : MonoBehaviour {

    const string MainMenu = "main_menu"; 
    const string MainGame = "main_game";
    const string CineMachine = "main_cinemachine";
    const string CineMachineMenu = "main_menu_cinemachine";

    public void LoadMainMenu()
    {
        SceneManager.LoadScene(CineMachineMenu, LoadSceneMode.Single);
        Time.timeScale = 1;
    }

    public void LoadMainGame()
    {
        SceneManager.LoadScene(MainGame, LoadSceneMode.Single);
        Time.timeScale = 1;
    }

    public void LoadCineMachine()
    {
        SceneManager.LoadScene(CineMachine, LoadSceneMode.Single);
        Time.timeScale = 1;
    }

    public void LoadCineMachineMenu()
    {
        SceneManager.LoadScene(CineMachineMenu, LoadSceneMode.Single);
        Time.timeScale = 1;
    }

    public void Restart()
    {
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name, LoadSceneMode.Single);
        Time.timeScale = 1;
    }

}
