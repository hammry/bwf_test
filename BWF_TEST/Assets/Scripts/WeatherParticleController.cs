﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeatherParticleController : MonoBehaviour {

    public ParticleSystem RainParticle;
    public float RainMaxEmissionRate = 100;
    Transform Camera;
    bool CameraFounded = false;


	void Start ()
    {
        if (GameObject.FindGameObjectWithTag("MainCamera") != null)
        {
            CameraFounded = true;

        }

        if (CameraFounded)
        {
            Camera = GameObject.FindGameObjectWithTag("MainCamera").transform;

            Update();
        }
            
        
	}
	

	void Update ()  
    {
        //Set Emiter Position
        if (CameraFounded)
        {
            transform.position = Camera.position;
        }
    }
}
