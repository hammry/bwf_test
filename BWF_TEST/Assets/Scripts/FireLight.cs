﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireLight : MonoBehaviour {

    Light FlameLight;
    float TargetIntensity;
    float CurrentIntensity;
    float StartIntensity;
    public float IntensityTime = 2f;
    float LinearTimer = 0f;

    void Start ()
    {
        FlameLight = this.GetComponent<Light>();
        StartIntensity = FlameLight.intensity;
        CurrentIntensity = StartIntensity;
        TargetIntensity = StartIntensity;
        InvokeRepeating("RandonIntensity", IntensityTime, IntensityTime);
    }
	

	void Update ()
    {
        LinearTimer += Time.deltaTime;
        FlameLight.intensity = Mathf.Lerp(CurrentIntensity, TargetIntensity, LinearTimer / IntensityTime);
    }

    void RandonIntensity()
    {
        CurrentIntensity = TargetIntensity;
        TargetIntensity = Random.Range(StartIntensity * 0.8f, StartIntensity * 1.2f);
        LinearTimer = 0;
    }
}
