﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjecPickup : MonoBehaviour {

    public GameObject Particle;
    public bool MoveWithPlayer = false;

    private void OnTriggerEnter(Collider touched_object)
    {
        GameObject NewParticle = (GameObject)Instantiate(Particle, transform.position, Quaternion.identity);
        NewParticle.AddComponent<ObjectLifetime>();
        NewParticle.GetComponent<ObjectLifetime>().Lifetime = 10;
        NewParticle.GetComponent<ObjectLifetime>().MoveWithPlayer = MoveWithPlayer;
        NewParticle.GetComponent<ObjectLifetime>().Player = touched_object.transform;

        if (this.CompareTag("Bonus"))
        {
            Data.Score += 10;
            Data.HP += 15f;
            Debug.Log("Bonus");
        }


        if (this.CompareTag("Damage"))
        {
            Data.HP -= 15f;
            Debug.Log("Damage");
        }


        Destroy(this.gameObject);
    }

}
