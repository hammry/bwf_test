﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameUiController : MonoBehaviour {

    public Text ScoreText;
    public Image HpBar;
    float HpPercentage;

	void Start ()
    {
		
	}
	

	void Update ()
    {
        ScoreText.text = "SCORE : " + Data.Score;
        HpPercentage = Data.HP / 100f;
        HpBar.fillAmount = HpPercentage;
        HpBar.color = Color.Lerp(Color.red, Color.green, HpPercentage);
    }
}
