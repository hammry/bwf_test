﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode] //for testing canvas without start game for all ratios and screen modes
public class CanvasAutoScaler : MonoBehaviour
{
    CanvasScaler _CanvasScaler;
    public Transform[] Transforms;
    float CanvasHeight, CanvasWidth;
    float CanvasAspectRatio;
    float CurrentAspectRatio;
    float AspectRatioFactor;
    float width, height;


    void Start()
    {
        _CanvasScaler = this.GetComponent<CanvasScaler>();
        CanvasHeight = _CanvasScaler.referenceResolution.y;
        CanvasWidth = _CanvasScaler.referenceResolution.x;
        CanvasAspectRatio = CanvasWidth / CanvasHeight;
    }


    void Update()
    {
        _CanvasScaler = this.GetComponent<CanvasScaler>();
        CanvasHeight = _CanvasScaler.referenceResolution.y;
        CanvasWidth = _CanvasScaler.referenceResolution.x;
        CanvasAspectRatio = CanvasWidth / CanvasHeight;
        width = Screen.width;
        height = Screen.height;
        CurrentAspectRatio = width / height;

        if (CurrentAspectRatio > CanvasAspectRatio)
        {
            AspectRatioFactor = CanvasAspectRatio / CurrentAspectRatio;
        }
        else
            AspectRatioFactor = 1;

        for (int i = 0; i < Transforms.Length; i++)
        {
            Transforms[i].localScale = new Vector3(AspectRatioFactor, AspectRatioFactor, 1);
        }


    }
}
