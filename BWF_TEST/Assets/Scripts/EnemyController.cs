﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour {

    NavMeshAgent Agent;
    Animator EnemyAnimator;
    public float EmenySpeed = 0.5f;
    public float SpeedRandimizer = 0.25f;


    void Start ()
    {
        Agent = this.GetComponent<NavMeshAgent>();
        EnemyAnimator = this.GetComponent<Animator>();
        float Randomizer = Random.Range(1f - SpeedRandimizer, 1f + SpeedRandimizer);
        Agent.speed = EmenySpeed * Randomizer;
        EnemyAnimator.speed = Agent.speed * 2;
    }
	

	void Update () 
    {
        Agent.SetDestination(Data.Player.position);
	}
}
