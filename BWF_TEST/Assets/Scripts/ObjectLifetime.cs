﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectLifetime : MonoBehaviour {

    public float Lifetime = 5;
    public bool MoveWithPlayer = false;
    public Transform Player;

    void Start ()
    {
        Invoke("DestroyThis", Lifetime);
    }
	
    void DestroyThis()
    {
        Destroy(this.gameObject);
    }

    void Update()
    {
        if(MoveWithPlayer)
        {
            transform.position = Player.position;
        }
    }

}
