﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public Image ColorChangeingImage;
    public Text ColorChangeingText;


    public void SetWhite()
    {
        if(ColorChangeingImage != null)
            ColorChangeingImage.color = Color.white;
        if (ColorChangeingText != null)
            ColorChangeingText.color = Color.white;
    }

    public void SetRed()
    {
        if (ColorChangeingImage != null)
            ColorChangeingImage.color = Color.red;
        if (ColorChangeingText != null)
            ColorChangeingText.color = Color.red;
    }

    public void SetGreen()
    {
        if (ColorChangeingImage != null)
            ColorChangeingImage.color = Color.green;
        if (ColorChangeingText != null)
            ColorChangeingText.color = Color.green;
    }

    public void SetBlue()
    {
        if (ColorChangeingImage != null)
            ColorChangeingImage.color = Color.blue;
        if (ColorChangeingText != null)
            ColorChangeingText.color = Color.blue;
    }

    public void SetGrey()
    {
        if (ColorChangeingImage != null)
            ColorChangeingImage.color = Color.gray;
        if (ColorChangeingText != null)
            ColorChangeingText.color = Color.gray;
    }

    public void Exit()
    {
#if    UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
         Application.Quit();
#endif
    }
}
