using System;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour
{
    private Toggle MenuToggle;
	private float TimeScaleRef = 1f;
    private float VolumeRef = 1f;
    private bool Paused;


    void Awake()
    {
        MenuToggle = GetComponent <Toggle>();
        Time.timeScale = TimeScaleRef;
        AudioListener.volume = VolumeRef;
    }


    private void MenuOn ()
    {
        TimeScaleRef = Time.timeScale;
        Time.timeScale = 0f;

        VolumeRef = AudioListener.volume;
        AudioListener.volume = 0f;

        Paused = true;
    }


    public void MenuOff ()
    {
        Time.timeScale = TimeScaleRef;
        AudioListener.volume = VolumeRef;
        Paused = false;
    }


    public void OnMenuStatusChange ()
    {
        if (MenuToggle.isOn && !Paused)
        {
            MenuOn();
        }
        else if (!MenuToggle.isOn && Paused)
        {
            MenuOff();
        }
    }


	void Update()
	{
		if(Input.GetKeyUp(KeyCode.Escape))
		{
		    MenuToggle.isOn = !MenuToggle.isOn;
            Cursor.visible = MenuToggle.isOn;//force the cursor visible if anythign had hidden it
		}
	}


}
